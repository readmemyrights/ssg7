# Copyright 2022, 2023 Seculum Forka
# This file is part of ssg7.
# ssg7 is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# ssg7 is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with ssg7. If not, see
# <https://www.gnu.org/licenses/>.
PREFIX?=/usr/local
BINDIR?=$(PREFIX)/bin
LIBEXECDIR?=$(PREFIX)/libexec/ssg7
MANDIR?=$(PREFIX)/share/man/man1
INSTALL?=install
SCRIPTS=ssg7 ssg7dir ssg7file
BINS=ssg7
LIBEXECS=ssg7dir ssg7file
MANPAGES=ssg7.1

# Note: when the help file gets longer than 24 lines, change cat to more.
help:
	@cat makefile-help.txt

install:
	mkdir -p $(BINDIR)
	$(INSTALL) -m 755 $(BINS) $(BINDIR)
	mkdir -p $(LIBEXECDIR)
	$(INSTALL) -m 755 $(LIBEXECS) $(LIBEXECDIR)
	mkdir -p $(MANDIR)
	$(INSTALL) -m 644 $(MANPAGES) $(MANDIR)

check:
	shellcheck $(SCRIPTS)

.PHONY: help install check
