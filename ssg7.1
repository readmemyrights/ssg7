.\" Copyright 2022, 2023 Seculum Forka
.\" This file is part of ssg7.
.\" ssg7 is free software: you can redistribute it and/or modify it under the
.\" terms of the GNU General Public License as published by the Free Software
.\" Foundation, version 3.
.\" ssg7 is distributed in the hope that it will be useful, but WITHOUT
.\" ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
.\" FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
.\" You should have received a copy of the GNU General Public License along with ssg7. If not, see
.\" <https://www.gnu.org/licenses/>.
.Dd 2023
.Dt SSG7 1
.Os
.Sh NAME
.Nm ssg7
.Nd static site generator
.Sh SYNOPSIS
.Nm
.Op Fl pr
.Oo
.Ar root
.Op Ar dest
.Oc
.Sh DESCRIPTION
.Nm
builds a static site out of files
in the directory provided by the first argument
.Pq "called the root"
and puts it into the directory
provided by the second argument
.Pq "called the destination" .
.Pp
If the first argument isn't provided,
the environment variable
.Ev $SITE_ROOT
will be used.
If its undefined or empty,
the current directory is used.
.Pp
If the second argument isn't provided
The environment variable
.Ev $SITE_DEST
will be used.
If it's not provided,
the directory
.Pa _build
under the website root will be used.
.Pp
The options are as follows:
.Bl -tag -width indent
.It Fl p
Process files and directories in parallel.
This option enables the
.Ev $SSG7_PARALLEL
environment variable
.Po
see
.Sx ENVIRONMENT
below.
.Pc .
.It Fl r
Forces rebuilding the website
no matter whether the source files were changed or not.
This option enables the
.Ev $SSG7_REBUILD
environment variable
.Po
see
.Sx ENVIRONMENT
below.
.Pc .
.El
See
.Sx CAVEATS
for an important note
regarding option processing.
.Pp
.Nm
goes through all the files
in the website root
ignoring all that start with a
.Sq \&. ,
a
.Sq _ ,
or any glob pattern listed in
.Pa .ssgignore .
If the file is a directory
it creates a directory with the same name
in the destination
and goes through files in it the same way,
unless it contains a file called
.Pa .noprocess ,
in which case it's copied straight to the destination
without the
.Pa .noprocess
file.
If its a file
it either gets converted to html
.Po
see
.Sx HTML CONVERSION
below
.Pc ,
or its copied as is to the destination.
Named pipes, sockets, and other kinds of files cause an error.
.Sh .ENV FILES
Before
.Nm
processes a directory
it sources a file called
.Pa .env
if it exists.
This file is a shellscript
which is ran by the same shell that interprets
.Nm ssg7 ,
probably
.Xr sh 1 .
This means that commands such as
.Ic export
and variable assignment
will effect the ssg7 process
modifying its behavior.
.Pp
It's usually used to change environment variables,
See
.Sx ENVIRONMENT
for a list.
For example:
.Pp
.Dl "export MARKDOWNFLAGS='--template blog'"
.Pp
will pass the
.Cm --template
option to the markdown processor.
If the processor is
.Xr pandoc 1
it will change the page template to
.Pa blog.html .
.Sh HOOKS
.Nm
can also run programs at certain points in the build process.
These programs are called
.Sy hooks
and they can be written in any programming language.
However, they are run as separate processes
so they can't change the environment of
.Nm
like
.Pa .env
files do.
.Pp
Pre hooks are run
just after sourcing the
.Pa .env
file.
If a
.Pa .pre
file exists and is executable,
It's executed
and is given the current source and destination directory
as its first and second command line argument respectively.
.Pp
Post hooks work in much the same way,
except they're run after all files have been processed,
and the file is called
.Pa .post
instead of
.Pa .pre .
.Sh PREPROCESSING
Before being converted to html,
files can be preprocessed by a program given in the
.Ev $PREPROCESSOR
environment variable.
It's output is piped
.Po
see
.Xr pipe 2
.Pc
into the program that converts it to html,
thus the program shouldn't convert it to html
or any other format the command doesn't expect.
.Pp
Usually the preprocesser makes simple changes to the file,
like replacing strings with information available at runtime
or removing specific sections.
Macro processors like
.Xr m4 1
or
.Xr cpp 1
can prove especially useful as preprocessors,
as would hand made
.Xr sed 1
scripts.
.Pp
Flags given to the preprocessor can be supplied in the
.Ev $PREPROCESSORFLAGS
environment variable.
.Sh HTML CONVERSION
The main advantage of static site generators
is their ability to convert
from more human-friendly formats like
.Sy markdown
to html.
This allows you to focus on your content,
and leave all the hard work of writing correct html
to the markdown to html converter.
.Pp
.Nm
supports converting from
.Sy markdown
and
.Sy asciidoc
and more can be added
by writing programs or shell functions called
.Cm ssg7file_ Ar ext
where
.Ar ext
is the extension associated with a filetype,
for example
.Pa ssg7file_org
would handle emacs org files.
Markdown files should end in
.Sy \&.md
and asciidoc files in
.Sy \&.adoc .
.Pp
Nearly every step of the conversion
can be influenced using environment variables
.Po
see
.Sx ENVIRONMENT
.Pc .
In general,
the variable for the program that performs the conversion is called
.Ev $FORMAT
where format is an all\-caps name of the formatlike
.Ev $MARKDOWN ,
and the variable giving the flags to be past to it would be
.Ev $FORMATFLAGS
for example
.Ev $MARKDOWNFLAGS .
.Pp
.Nm
doesn't reconvert the file
if it wasn't changed since the last time
a conversion was performed.
A reconversion can be forced
by deleting the destination file,
or setting the
.Ev $SSG7_REBUILD
environment variable
to reconvert all files.
.Sh ENVIRONMENT
Note that many of these variables
are interpreted as shell code,
so be sure to quote any necessary meta-characters.
This is so that word splitting is properly performed,
and to be able to have arguments containing spaces.
.Pp
Some of these variables enable or disable a certain feature or action,
and they are commonly referred to as
.Sy booleans .
If they are
.Em unset
or
.Em empty
The feature is disabled,
otherwise the feature is enabled.
.Bl -tag -width indent
.It Ev "$SITE_ROOT"
The root of the website
if not specified on the command line.
The current directory is used if unset.
.It Ev "$SITE_DEST"
The directory where the finished website is output.
The
.Pa _build
subdirectory of the website root is used if unset.
.It Ev $PREPROCESSOR
The program ran on markdown and asciidoc files
before being converted to html.
default is
.Xr cat 1 .
.It Ev $MARKDOWN
The program used for converting markdown files to html.
Default is
.Xr pandoc 1 .
.It Ev $ASCIIDOC
The program used for converting asciidoc files to html.
Default is
.Xr asciidoctor 1 .
.It Ev $PREPROCESSORFLAGS , Ev $MARKDOWNFLAGS , Ev $ASCIIDOCFLAGS
flags past to
.Ev $PREPROCESSOR ,
.Ev $MARKDOWN ,
and
.Ev $ASCIIDOC .
.It Ev $SSG7_PARALLEL
Processes multiple files and directories in parallel.
While this option reduces the amount of time needed for the website to be built,
It's rather experimental and as of time of writing this,
there is no limit on the amount of processes spawned,
except the system limit.
.It Ev $SSG7_REBUILD
Rebuilds the website fully,
with no regard whether the source files were changed or not.
.El
.\" .Sh FILES
.\" .Sh EXAMPLES
.Sh EXIT STATUS
.Ex -std ssg7
.Sh SEE ALSO
.Xr asciidoctor 1 ,
.Xr cat 1 ,
.Xr cpp 1 ,
.Xr find 1 ,
.Xr m4 1 ,
.Xr pandoc 1 ,
.Xr sed 1 ,
.Xr sh 1 ,
.Xr ssg7pp 1 ,
.Xr markdown 7
.Sh AUTHORS
.An "Seculum Forka"
.Aq "dictatorofturkmenistan@protonmail.com"
.Sh CAVEATS
.Nm
can't properly handle options
without gnu-getopt.
If gnu-getopt isn't available,
options must be supplied individually,
and option arguments must be separated from the option
with a space.
.Pp
Before version 0.2,
.Ev $PREPROCESSOR
got the file it processed
as its first argument.
Version 0.2 onward gives it the data on
.Sy "standard input" .
.Pp
Since as of version 0.21
.Cm ssg7file
doesn't source any files,
and posix shell doesn't export functions to the environment,
adding filetype handlers can only be done via external programs.
This might change in the future.
.Sh BUGS
Files deleted in the source directory
don't get deleted in the destination directory automatically.
.Pp
Report any you encounter at
.Aq "https://codeberg.org/readmemyrights/ssg7" .
