# ssg7

ssg7 is a simple static site generator written in posix sh. It supports markdown
using [pandoc](https://www.pandoc.org/) and asciidoc using
[asciidoctor](https://asciidoctor.org/).

While it's still a work in progress, the most important features should work
just fine, and it should be usable for most people.

Apart from standard unix utilities, ssg7 doesn't have any dependencies. However,
it's operation is enhanced by the following tool(s).

* gnu-getopt: for option processing. See the CAVEATS section in the man page for
  more information.

## Why use this over hugo, jekyll, etc

* It's small. All together it's about 100 lines of shell, and almost a third of
  it are license notices and shebangs.
* It's easy to get started with. Just run it in a directory containing markdown
  files, and all the html is in the _build folder
* However, if you want to customize it, you can do that just by setting an
  environment variable or two. You aren't forced into using a specific
  templating language, learning a website design paradigm, or adopting a life
  philosophy, everything is how you want it.
* It's easy to extend and to hack on.

## Examples

Take a look at the example directories if you want to add additional
capabilities to your static website. You might have to modify some of them to
work on your system. If you wish to contribute your own scripts take a look at
[EXAMPLES.md](EXAMPLES.md).

## Installation

```shell
make install # might require sudo
make PREFIX=/usr install # change the prefix
make BINDIR=/command MANDIR=/documentation install # select where the shellscripts and man pages will be installed.
```

For the list of all options run `make help`.

## Contributing

Pull requests are welcome, as is any other help.
For things that need attention see [TODO.md](TODO.md).

Be sure of the following:

* This project uses [editorconfig](https://editorconfig.org/) to enforce style
  guidelines, so make sure your editor supports it.
* Run `make check` before committing and sending the pull request. It requires
  [shellcheck](https://shellcheck.net/). to make sure there aren't any errors.
* The documentation is written in nroff using mdoc macros. Please follow the
  style recommendation in the mdoc man page as well as you can unless said
  otherwise. If you aren't familiar with nroff or mdoc macros, feel free to send
  a plain text version of what you want to add and I or someone else will
  convert it. Be sure to use `mandoc -Wall` to catch any errors!
* In ssg7dir any function or variable names used before sourcing the .env
  file should start and end with two underscores `__`.
