# Things to work on

* [ ] Unite ssg7{,file,dir} into a single program.
  Has a potential to improve performance but would cause issues with some
  features, in particular .env files, all of their effects would stack up in a
  single process.
* [ ] ssg7file: source user's shell function handlers. How much more performent
  would they be compared to external programs? Keep in mind that as things stand
  now, that file would be sourced for every file handled, and that posix sh
  doesn't export functions
* [ ] Manpages for helper programs? Might not be as useful to users, but would
  be useful for future developers once the programs become full on unreadable
* [ ] Populate the examples directory, and oh decide what to do about the
  ssg7pp.1 lingering around in the root folder.
