# Examples

The examples directory contains various scripts to add more features to ssg7.
Most are suppose to be run as hooks, either being the .pre/.post files
themselves or being called from them with the original first and second
arguments.

if you wish to add any scripts you find useful, or improve the ones already
there, feel free to open an issue.

The scripts are meant to be simple, short, and easily customizable, just like
the rest of the ssg7 codebase. However, they aren't meant to be complete nor
fully portable, instead serving as inspirations for your own scripts. Only
report a bug if there is a serious error in the logic of the program.

Shellcheck can be used on scripts written in sh but it isn't required. In
general it's recommended however to catch unintended mistakes. Note that
`make check` doesn't automatically check scripts in the example directory.
